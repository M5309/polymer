//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.use(express.static(__dirname + '/build/default')); //Modificamos linea y dejamos el default para que cada que se haga build, no se tenga que dar el nombre

app.listen(port);

console.log('Ejecutando Polymer desde node: ' + port);

app.get ("/", function(req, res){
  res.sendFile("index.html", {root: '.'}); //direccionamos al index del root
});
