# Imagen base
FROM node:latest

#Directorio de la app (aplicaciòn)
WORKDIR /app

#Copiado de archivos al dir de la aplicación desde donde estoy parado
ADD build/default /app/build/default
ADD server.js /app
ADD package.json /app  

#Dependencias
RUN npm install

#Puerto que se expone
EXPOSE 3000

#Comando con el que se ejecuta la aplicación
CMD ["npm", "start"]
